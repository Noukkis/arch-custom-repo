FROM archlinux:base-devel

RUN pacman -Syu --noconfirm devtools python python-pip

RUN useradd --create-home --shell=/bin/false build \
    && usermod -L build \
    && echo 'build ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
    
RUN git clone https://aur.archlinux.org/aurutils.git /aurutils \
    && chown -R build:build /aurutils \
    && cd /aurutils \
    && sudo -u build makepkg -si --noconfirm \
    && cd / && rm -rf /aurutils

WORKDIR /app
ADD requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
ADD src .

RUN chmod +x /app/entrypoint.py /app/init.sh /app/sync.sh /app/sync_git.sh \
    && ln -s /app/sync.sh /usr/bin/repo-sync \
    && mkdir /data \
    && echo 'aurutils' > /data/aur.txt \
    && echo -e '\n[custom]\nServer = file:///srv/http/arch/x86_64' >> /etc/pacman.conf

USER build
EXPOSE 80

CMD /app/entrypoint.py