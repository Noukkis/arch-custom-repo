#!/usr/bin/env sh

if [ ! -e "/srv/http/arch/x86_64/custom.db.tar.zst" ]; then
    sudo mkdir -p /srv/http/arch/x86_64
    sudo chown -R build:build /srv/http
    repo-add --remove /srv/http/arch/x86_64/custom.db.tar.zst
fi