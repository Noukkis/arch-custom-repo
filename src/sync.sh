#!/usr/bin/env sh
AUR_FILE=/data/aur.txt
GIT_FILE=/data/git.txt

echo '######################'
echo '# SYNC ARCH PACKAGES #'
echo '######################'
sudo pacman -Syu --noconfirm

echo '######################'
echo '#      SYNC AUR      #'
echo '######################'
if [[ -f "$AUR_FILE" ]]; then
    aur sync --noview --noconfirm -d custom $(cat "$AUR_FILE")
else
    echo "file '$AUR_FILE' doesn't exist"
fi

echo '######################'
echo '#      SYNC GIT      #'
echo '######################'
if [[ -f "$GIT_FILE" ]]; then
    while read line; do
        /app/sync_git.sh $line
    done < "$GIT_FILE"
else
    echo "file '$GIT_FILE' doesn't exist"
fi