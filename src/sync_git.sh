BASE_DIR=/data/git
mkdir -p "$BASE_DIR"

repo=$1
base=$(basename "$repo")
dir="$BASE_DIR/${base%.*}"

sync_now() {
    cd "$dir"
    git clean -fdx
    git pull
    makepkg -s --noconfirm
    repo-add --remove /srv/http/arch/x86_64/custom.db.tar.zst *.zst
    cp *.zst /srv/http/arch/x86_64
}

if [[ -d "$dir" ]]; then
    cd "$dir"
    git remote update
    LOCAL=$(git rev-parse @)
    REMOTE=$(git rev-parse @{u})
    if [ $LOCAL != $REMOTE ]; then
        sync_now
    fi
else
    git clone "$repo" "$dir"
    sync_now
fi