#!/usr/bin/env python
from threading import Thread
from http.server import HTTPServer, SimpleHTTPRequestHandler
from croniter import croniter
from datetime import datetime, timedelta
from time import sleep
from os import system, environ

SCHEDULE_ENV = 'SYNC_SCHEDULE'
SYNC_CMD = '/app/sync.sh'
SERVER_DIR = '/srv/http'

INIT_CMD = '/app/init.sh'

class HTTPRequestHandler(SimpleHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, directory=SERVER_DIR, **kwargs)

def main():
    system(INIT_CMD)
    http_thread = Thread(target=http_serve)
    http_thread.start()

    sync()
    sched = environ[SCHEDULE_ENV] if SCHEDULE_ENV in environ else '0 0 * * *'
    schedule(sched, sync)

def http_serve():
    httpd = HTTPServer(('', 80), HTTPRequestHandler)
    httpd.serve_forever()

def schedule(sched, func):
    nextRunTime = getNextCronRunTime(sched)
    while True:
        roundedDownTime = roundDownTime()
        if (roundedDownTime == nextRunTime):
            func()
            nextRunTime = getNextCronRunTime(sched)
        elif (roundedDownTime > nextRunTime):
            nextRunTime = getNextCronRunTime(sched)
        sleepTillTopOfNextMinute()

def sync():
    print('SYNC START')
    system(SYNC_CMD)
    print('SYNC FINISHED')

def roundDownTime(dt=None, dateDelta=timedelta(minutes=1)):
    roundTo = dateDelta.total_seconds()
    if dt == None : dt = datetime.now()
    seconds = (dt - dt.min).seconds
    rounding = (seconds+roundTo/2) // roundTo * roundTo
    return dt + timedelta(0,rounding-seconds,-dt.microsecond)

def getNextCronRunTime(schedule):
    return croniter(schedule, datetime.now()).get_next(datetime)

def sleepTillTopOfNextMinute():
    t = datetime.utcnow()
    sleeptime = 60 - (t.second + t.microsecond/1000000.0)
    sleep(sleeptime)

if __name__ == '__main__':
    main()